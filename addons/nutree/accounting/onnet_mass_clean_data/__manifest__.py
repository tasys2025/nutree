# -*- coding: utf-8 -*-
{
    'name': "Onnet - Mass Clean Data",

    'summary': """""",

    'description': """""",

    'author': "SonPT",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': '',
    'version': '13.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/mass_clean_data_view.xml',
        'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'installable': True,
    'application': False,
}
