{
    'name': 'Sales Purchase Button Plus Minus Quantity',
    'version': '1.0',
    'category': 'Sales Management',
    'description': """
Add Button to Sales And Purchase.
    * Add Button + and - at sales order line for qty
    * Add Button + and - at Purcase order line for qty
""",
    'author': 'Tung.nt@tasys.vn',
    'website': '',
    'images': [],
    'depends': ['sale'],
    'data': [
        'views/sale_views.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}