# -*- coding: utf-8 -*-
{
    'name': 'BMS LAB JS',
    'version': '1.0',
    'author': 'Pham Ngoc Thach',
    'category': 'TASYS',
    'website': "http://tasys.vn",
    'summary': """
        Base JS LIBRARY FOR TASYS REPORT
    """,
    'depends': ['base'],
    'data': [
        "views/templates.xml",
    ],
    'images': [
        'static/description/icon.png',
    ],
    'installable': True,
    'auto_install': False,
}
