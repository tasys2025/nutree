{
    "name": "Report Customization",
    "license": "AGPL-3",
    "author": "taiht",
    "depends": ["base","bi_professional_reports_templates"],
    "data": [
        "views/invoice_report.xml",
        "views/account_invoice_report.xml",
        "views/sale_oder_view.xml",
        "views/sales_order_report.xml",
    ],
    "installable": True,
}
