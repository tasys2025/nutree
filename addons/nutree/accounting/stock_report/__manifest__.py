
{
    "name": "stock report customization",
    "license": "AGPL-3",
    "summary": "receipt (in) report and delivery (out) report ",
    "author": "taiht",
    "website": "https://github.com/OCA/account-financial-tools",
    "depends": ["stock","account"],
    "data": [
        "report/report_stockpicking.xml",
        "report/picking_list_request_report.xml",
        "report/stock_report_views.xml",
    ],
    "installable": True,
}
