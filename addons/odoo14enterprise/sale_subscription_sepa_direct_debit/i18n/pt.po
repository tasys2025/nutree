# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* sale_subscription_sepa_direct_debit
# 
# Translators:
# Martin Trigaux, 2020
# Manuela Silva <manuelarodsilva@gmail.com>, 2020
# Reinaldo Ramos <reinaldo.ramos@arxi.pt>, 2020
# Pedro Filipe <pedro2.10@hotmail.com>, 2020
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~13.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-01 07:40+0000\n"
"PO-Revision-Date: 2020-09-07 08:23+0000\n"
"Last-Translator: Pedro Filipe <pedro2.10@hotmail.com>, 2020\n"
"Language-Team: Portuguese (https://www.transifex.com/odoo/teams/41243/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: sale_subscription_sepa_direct_debit
#: model:ir.model.fields,field_description:sale_subscription_sepa_direct_debit.field_payment_transaction__display_name
#: model:ir.model.fields,field_description:sale_subscription_sepa_direct_debit.field_sale_subscription__display_name
msgid "Display Name"
msgstr "Nome"

#. module: sale_subscription_sepa_direct_debit
#: model:ir.model.fields,field_description:sale_subscription_sepa_direct_debit.field_payment_transaction__id
#: model:ir.model.fields,field_description:sale_subscription_sepa_direct_debit.field_sale_subscription__id
msgid "ID"
msgstr "ID"

#. module: sale_subscription_sepa_direct_debit
#: model:ir.model.fields,field_description:sale_subscription_sepa_direct_debit.field_payment_transaction____last_update
#: model:ir.model.fields,field_description:sale_subscription_sepa_direct_debit.field_sale_subscription____last_update
msgid "Last Modified on"
msgstr "Última Modificação em"

#. module: sale_subscription_sepa_direct_debit
#: model:ir.model,name:sale_subscription_sepa_direct_debit.model_payment_transaction
msgid "Payment Transaction"
msgstr "Transação de Pagamento"

#. module: sale_subscription_sepa_direct_debit
#: model:ir.model,name:sale_subscription_sepa_direct_debit.model_sale_subscription
msgid "Subscription"
msgstr "Subscrição"
